Minio in Swarm
=========
Deploys MinIO in Docker Swarm.

Requirements
------------
Docker Swarm cluster is present  

Include role
------------
```yaml
- name: docker
  src: https://gitlab.com/ansible_roles_v/minio_in_swarm
  version: main
```

Example Playbook
----------------
```yaml
- hosts: servers
  roles:
    - minio
```

Warning
=========
Molecule tests don't works!
It will be fixed in the future versions.
